(ns cljserial.utils.hsm
  (:require [statecharts.core :as hsm]
            [lambdaisland.glogi :as log]
            [cljserial.utils.dbfx :as dbfx]
            [cljserial.utils.collections :as c-utils]))

; See https://lucywang000.github.io/clj-statecharts/

;-- Schema (malli)
(def Id :keyword)
(def State [:sequential :keyword])

;; Context for a single statemachine
(def Context
  [:map
   ;;Context can include additional keys -- malli schema is open, so not a problem
   [:_state State]])


;-- Helper functions

(defn get-state [context]
  (:_state context))

(defn in-state [current-state match-state]
  (some #(= match-state %) (flatten [current-state])))

(defn- get-state-events [state]
  (if (contains? state :on)
    (keys (:on state))
    nil))

(defn- find-machine-events
  [top-state]
  (->> (c-utils/find-all top-state :on)
       (map keys)
       flatten
       set))

(defn- recurse-state-events
  ([parent-state state-v] (recurse-state-events parent-state (flatten [state-v]) []))
  ([parent-state state-v event-v]
   (if (empty? state-v)
     (set event-v) ; we're done
     (let [[state-v-first & state-v-rest] state-v
           next-state (state-v-first (:states parent-state))
           event-v-new (reduce conj event-v (get-state-events next-state))]
       (recur next-state state-v-rest event-v-new)))))

(defn handled-events
  ([machine] (find-machine-events machine))
  ([machine state-v] (recurse-state-events machine state-v)))

(defn can-handle [machine state-v event-id]
  (contains? (handled-events machine state-v) event-id))

(defn- handle [hsm-id hsm-impl event hsm-context]
  (let [[event-id & event-params] event
        state (get-state hsm-context)]
    (if (not (can-handle hsm-impl state event-id))
      (do
        (log/error :handler/no-handler-this-state (str hsm-id "@" state "/" event-id " not handled!"))
        hsm-context)
      (let [new-context (hsm/transition hsm-impl hsm-context {:type event-id :data event-params})
            new-state (get-state new-context)]
        (log/debug :handle/transition (str hsm-id "@" state "/" event-id "->" new-state))
        (log/trace :handle/event event)
        new-context))))

;; re-frame / dbfx integration

(defn sub-name [hsm-id sub-tag]
  (keyword (str (name hsm-id) "-" sub-tag)))

(defn sub-context-name [hsm-id]
  (sub-name hsm-id "context"))

(defn sub-state-name [hsm-id]
  (sub-name hsm-id "state"))

(dbfx/reg-event-db
 :hsm-init
 (fn [app-db [_event_id hsm]]
   (let [hsm-id (:id hsm)
         context (hsm/initialize hsm)
         state (get-state context)]
     (log/config :init/transition (str hsm-id "/" _event_id "->" state))
     (log/trace :init/context context)
     (assoc-in app-db [hsm-id] context))))

;;-- ... subscriptions

;;-- .... layer 2 subscriptions
(defn reg-sub-context
  "Define a subscription for the context of a given statemachine ID"
  [hsm-id]
  (let [sub-name (sub-context-name hsm-id)]
    (log/config :reg-sub/context hsm-id)
    (dbfx/reg-sub
     sub-name
     (fn [app-db _]
     ;;TODO: Add check for existence of hsm-id key?
       (get app-db hsm-id)))))

;;-- ..... layer 3 subscription
(defn reg-sub-state
  "Define a subscription for the state of a given statemachine ID"
  [hsm-id]
  (let [sub-name (sub-state-name hsm-id)]
    (log/config :reg-sub/state hsm-id)
    (dbfx/reg-sub
     sub-name
     :<- [(sub-context-name hsm-id)]
     (fn [hsm-context _]
       (get-state hsm-context)))))

(defn reg-event-handler
  [hsm-id event-id hsm-impl]
  (log/info :hsm-event-handler (str "Registering " event-id " handler for " hsm-id))
  (dbfx/reg-event-db
   event-id
   (dbfx/path [hsm-id]) ;hsm context path interceptor
   (fn [hsm-context event]
     (handle hsm-id hsm-impl event hsm-context))
   ))

(defn reg-event-handlers
  [hsm-data]
  (let [hsm-id (:id hsm-data)
        event-ids (handled-events hsm-data)]
    (log/info :register/handlers (str "Registering " (count event-ids) " event handlers for " hsm-id))
    (doseq [event-id event-ids]
      (reg-event-handler hsm-id event-id hsm-data))))

;; API
(defn register [hsm-data]
  (let [hsm-id (:id hsm-data)]
    (log/config :register/on-dom-content-loaded hsm-id)
    (.addEventListener js/document "DOMContentLoaded" #(dbfx/dispatch [:hsm-init hsm-data]))

    (reg-sub-context hsm-id)
    (reg-sub-state hsm-id)
    (reg-event-handlers hsm-data)))

(defn use-sub-context [hsm-id]
  (dbfx/use-sub [(sub-context-name hsm-id)]))

(defn use-sub-state [hsm-id]
  (dbfx/use-sub [(sub-state-name hsm-id)]))
