(ns cljserial.utils.dbfx
  "Facade to allow switching between refx and re-frame for app state management.
  Probably don't want to do this long term, but keeping options open for now.
  Re-frame is more actively maintained and has some nice dev tooling (re-frame 10x etc).
  refx is an adaptation to use more recent react conventions.
  Might want to do comparative performance evaluation at some point (though likely a non-issue for our level of complexity). "
  (:require [refx.alpha :as refx]
            [refx.interceptor]
            [refx.interceptors]
            [re-frame.core :as rf]
            [cljserial.utils.uix-reframe :as rf-utils]
            [cljserial.utils.browser :as browser]
            [cljserial.utils.schema :as schema-utils]))

(defonce impl :re-frame)


(if (= impl :refx)
  (do ;; refx aliases
    (def reg-sub refx/reg-sub)
    (def use-sub refx/use-sub)
    (def dispatch refx/dispatch)
    (def dispatch-sync refx/dispatch-sync)
    (def reg-fx refx/reg-fx)
    (def reg-cofx refx/reg-cofx)
    (def reg-event-fx refx/reg-event-fx)
    (def reg-event-db refx/reg-event-db)
    (def inject-cofx refx/inject-cofx)

    ;; The global interceptor -- instrument all published events
    (def ->interceptor refx.interceptor/->interceptor)
    ;;FIXME: Does this exist for refx?
    (def reg-global-interceptor nil)

    (def path refx.interceptors/path)
    (def after refx.interceptors/after))
  (do ;; re-frame aliases
    (def reg-sub rf/reg-sub)
    (def use-sub rf-utils/use-subscribe)
    (def dispatch rf/dispatch)
    (def dispatch-sync rf/dispatch-sync)
    (def reg-fx rf/reg-fx)
    (def reg-cofx rf/reg-cofx)
    (def reg-event-fx rf/reg-event-fx)
    (def reg-event-db rf/reg-event-db)
    (def inject-cofx rf/inject-cofx)

    ;; The global interceptor -- instrument all published events
    (def ->interceptor rf/->interceptor)
    (def reg-global-interceptor re-frame.core/reg-global-interceptor)

    (def path rf/path)
    (def after rf/after)))


(defn schema-check-interceptor
  "Defines a refx interceptor that validates updated db content against a given (malli) `schema`"
  [schema]
  (after (partial schema-utils/check-and-throw schema)))

(defn browser-cache-interceptor
  "Defines a refx interceptor that caches data to browser local storage under the specified `id`"
  [id] (after #(browser/write {:id id :data %})))
