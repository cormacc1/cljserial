(ns cljserial.utils.bt
  "Utilities and Malli schema relating to the Web Bluetooth API.

  See the following references:
  - https://developer.mozilla.org/en-US/docs/Web/API/Bluetooth
  - https://github.com/WebBluetoothCG/web-bluetooth
  - https://webbluetoothcg.github.io/web-bluetooth/
  N.B. the WebBluetoothCG docs have more detail, but the Mozilla docs are more readable in many cases

  Valid identifiers etc. are made available by the SIG in YAML format in a public GIT repo here:
  https://bitbucket.org/bluetooth-SIG/public/src/main/assigned_numbers/
  Could potentially read this at build or run time and integrate here....
  "
  (:require [malli.core :as m]))

;; === Schema ====

;; Placeholder / minimum Malli schema for UI prototype

(def CompanyIdentifier [:int {:min 0 :max 65535}])

(def DeviceFilter :string);;FIXME: Placeholder -- declare properly

(def Uuid [:re #"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"])
(def UuidAlias [:re #"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"]);;FIXME: This should be a 16 or 32-bit alias to a 128-bit Uuid

;;Defined Name corresponding to a Service, Characteristic or Descriptor Uuid
(def Name [:re #"^[a-z0-9_-]+$"])

;; A GATT service/characteristic/descriptor may be referenced by UUID, UUID alias, or registered name
;; See https://github.com/WebBluetoothCG/registries/tree/master for the lists of registered names.
(def GattId [:or Uuid UuidAlias Name])

(def ServiceUuid Uuid);;TODO Specialise
(def CharacteristicUuid Uuid);;TODO Specialise
(def DescriptorUuid Uuid);;TODO: Specialise

;; TODO: May be simpler to specify (empty) defaults for the optional components for our purposes
;;       Rather than have lots of (if (empty? ..)) type logic in UI components
;;       That's assuming the api functions don't choke on empty array args....
(def DeviceRequestOptions [:map
                           :filters {:optional true} [:sequential DeviceFilter]
                           :exclusionFilters {:optional true} [:sequential DeviceFilter]
                           :optionalServices {:optional true} [:sequential GattId]
                           :optionalManufacturerData {:optional true} [:sequential CompanyIdentifier]
                           :acceptAllDevices {:optional true} :boolean])
;; TODO: Populate with sensible defaults...
(def ^:const DEFAULT_OPTIONS {})
