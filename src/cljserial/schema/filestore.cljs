(ns cljserial.schema.filestore)

;;
(def Path [:string {:min 1 :max 12}]) ;; for now anyway

(def File
  [:map
   [:path Path]
   [:attributes [:string {:min 4 :max 4}]]
   [:size [:int (:min 0)]]
   [:timestamp [:string {:min 12 :max 12}]] ;;TODO Set this do a datetime type...
   [:bytes :string]])

;;Use path as unique ID
(def FileMap
  [:map-of Path File])

(def FileStoreInfo
  [:map
   [:capacity-mb :int]
   [:used-mb :int]])

(def FileStore
  [:map
   [:info FileStoreInfo]
   [:files FileMap]])
