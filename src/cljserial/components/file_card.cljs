(ns cljserial.components.file-card
  (:require
   [cuerdas.core :as str]
   [uix.core :as uix :refer [defui $]]
   [malli.core :as m]
   [cljserial.components.cards :as cards]
   [cljserial.components.tables :as tables :refer [td]]
   [cljserial.schema.filestore :refer [FileStore]]))

(defui card [{:keys [filestore on-file-click]}]
  {:pre [m/validate FileStore filestore]}
  (let [info (:info filestore)
        files (:files filestore)]
    ($ cards/card {:title "Files"}
       ($ tables/table
          ($ :thead ($ :tr ($ :th "name") ($ :th "size")))
          ($ :tbody
             (for [f (vals files)]
               (let [filename (:path f)]
                 ($ :tr {:key filename :on-click (fn [^js _] (on-file-click filename))} ($ td filename) ($ td (:size f)))))
             ($ :tr ($ td "Storage") ($ td (str/format "%s / %s MB" (:used-mb info) (:capacity-mb info)))))))))
