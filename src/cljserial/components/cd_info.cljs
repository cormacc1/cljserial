(ns cljserial.components.cd-info
  "A generic info component for the MBT and MBX Controllers"
  (:require
   [cuerdas.core :as str]
   [uix.core :as uix :refer [defui $]]
   [malli.core :as m]
   [cljserial.components.cards :as cards]
   [cljserial.components.tables :as tables :refer [td]]
   [cljserial.schema.version :as version]
   [cljserial.services.mbt-cd :as mbt-cd]))

(defui card [cd-info]
  {:pre [m/validate mbt-cd/CdState cd-info]}
  ($ cards/card {:title "Controller Info"}
     ($ tables/table
        ($ :tbody
           ($ :tr ($ td "Serial #") ($ td (:serial cd-info)))
           ($ :tr ($ td "Hardware Revision") ($ td (:hardware-revision cd-info)))
           ($ :tr ($ td "Firmware Revision")  ($ td (version/->string (:firmware-revision cd-info))))
           ($ :tr ($ td "Build Configuration") ($ td (:build-configuration cd-info)))
           ($ :tr ($ td "Track") ($ td (:track cd-info)))
           (let [sd-info (:info (:sd-card cd-info))
                 files (:files (:sd-card cd-info))]
             ($ :tr ($ td "Storage") ($ td (str/format "%s / %s MB" (:used-mb sd-info) (:capacity-mb sd-info))))
             ($ :tr ($ td "Files") ($ td (count files))))
           ;; END
           ))))
