(ns cljserial.components.bt-device
  "A WebBluetooth device settings UI component."
  (:require
   [lambdaisland.glogi :as log]
   [uix.core :as uix :refer [defui $]]
   [malli.core :as m]
   [cljserial.utils.bt :as bt]
   [cljserial.components.cards :as cards]))

(defui settings
  [{:keys [device options]}]
  ;; FIXME: Bypassing for now because an empty map is causing a validation error, despite all elements being optional....
  ;;        Revisit malli docs...
  ;; {:pre [(m/validate bt/DeviceRequestOptions options)]}

  ($ cards/card {:title "Bluetooth settings"}
     (if (nil? device)
       ($ :div "TODO: No device connected")
       ($ (str "TODO: Connected to " device)))))
