(ns cljserial.pages.bt-terminal
  (:require
   [lambdaisland.glogi :as log]
   [uix.core :refer [defui $]]
   [cljserial.utils.dbfx :as dbfx]
   [cljserial.utils.hsm :as hsm-dbfx]
   [cljserial.components.bt-device :as bt-device]
   [cljserial.components.cd-info :as cd-info]
   [cljserial.components.file-card :as file-card]
   [cljserial.components.term :as term]))


(defui term-widget-dbfx [{:keys [event-sub tx-event-id] :as props}]
  (let [events (dbfx/use-sub [event-sub])]
    ($ term/term-widget {:events events
                         :on-add-event #(when (seq %)
                                          (dbfx/dispatch [tx-event-id %]))})))

;; (defn port-request-callback [vid _e]
;;   (wsu/await-port
;;    :vendor-id vid
;;    :on-success #(dbfx/dispatch [:bluetooth-has-device %1])
;;    :on-failure #(dbfx/dispatch [:bluetooth-no-device])))

(defui bluetooth-unsupported-message []
  ($ :div "The Web Bluetooth API is not currently available. Make sure you're running Chrome, Edge or Opera and serving this page from a secure context (i.e. over a https link or from localhost).  See Mozilla documentation for more details on " ($ :a {:href "https://developer.mozilla.org/en-US/docs/Web/API/Bluetooth#browser_compatibility"} "browser support") ", and " ($ :a {:href "https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts"} "secure contexts") "."))

(defui side-panel []
  ($ :.flex.flex-col
     (let [bt-context (hsm-dbfx/use-sub-context :bt-serial)
           device (:device bt-context)
           bt-options (:options bt-context)]
       (log/debug :bt-terminal/options bt-options)
       ($ bt-device/settings
          {:device device
           :options bt-options}))
     (let [cd-info (dbfx/use-sub [:cd-info])
           sd-info (:sd-card cd-info)]
       [($ cd-info/card cd-info)
        ($ file-card/card {:filestore sd-info :on-file-click #(dbfx/dispatch [:bt-tx (str "file get " %)] )})])))

(defui terminal-pane []
  ($ :.flex.flex-col
     (let [bt-state (hsm-dbfx/use-sub-state :bt-serial)]
       (log/debug :terminal/layout (str "Rendering hsm state " bt-state))
       (if (hsm-dbfx/in-state bt-state :no-bluetooth)
         ($ bluetooth-unsupported-message)
         ;;FIXME: Update this to subscribe to the bt-serial-events store instead...?
         ($ term-widget-dbfx {:event-sub :serial-events :tx-event-id :bt-tx})))))

(defui layout []
  ($ :.flex.flex-row.gap-1
     ($ :.basis-80.grow-0 ($ side-panel))
     ($ :.basis-0.grow
        ($ terminal-pane))))
