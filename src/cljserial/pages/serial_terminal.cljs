(ns cljserial.pages.serial-terminal
  (:require
   [lambdaisland.glogi :as log]
   [uix.core :refer [defui $]]
   [cljserial.utils.dbfx :as dbfx]
   [cljserial.utils.hsm :as hsm-dbfx]
   [cljserial.utils.webserial :as wsu]
   [cljserial.components.cd-info :as cd-info]
   [cljserial.components.file-card :as file-card]
   [cljserial.components.serial-port :as serial-port]
   [cljserial.components.term :as term]))


(defui term-widget-dbfx [{:keys [event-sub tx-event-id] :as props}]
  (let [events (dbfx/use-sub [event-sub])]
    ($ term/term-widget {:events events
                         :on-add-event #(when (seq %)
                                          (dbfx/dispatch [tx-event-id %]))})))

(defn port-request-callback [vid _e]
  (wsu/await-port
   :vendor-id vid
   :on-success #(dbfx/dispatch [:webserial-has-port %1])
   :on-failure #(dbfx/dispatch [:webserial-no-port])))

(defui webserial-unsupported-message []
  ($ :div "The Web Serial API is not currently available. Make sure you're running Chrome, Edge or Safari and serving this page from a secure context (i.e. over a https link or from localhost).  See Mozilla documentation for more details on " ($ :a {:href "https://developer.mozilla.org/en-US/docs/Web/API/Web_Serial_API#browser_compatibility"} "browser support") ", and " ($ :a {:href "https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts"} "secure contexts") "."))

(defui side-panel []
  ($ :.flex.flex-col
     (let [port-context (hsm-dbfx/use-sub-context :usb-serial)
           port (:port port-context)
           serial-options (:serial-options port-context)]
       (log/debug :terminal/options serial-options)
       ($ serial-port/settings
          {:port (if port
                   (wsu/describe-port port)
                   nil)
           :options serial-options
           :on-port-request (partial port-request-callback (:vendorIdFilter serial-options))
           :on-port-forget #(dbfx/dispatch [:webserial-forget-port])
           :on-option-update (fn [k v]
                               (dbfx/dispatch [:webserial-option k v]))}))
     (let [cd-info (dbfx/use-sub [:cd-info])
           sd-info (:sd-card cd-info)]
       [($ cd-info/card cd-info)
        ($ file-card/card {:filestore sd-info :on-file-click #(dbfx/dispatch [:serial-tx (str "file get " %)] )})])))

(defui terminal-pane []
  ($ :.flex.flex-col
     (let [serial-state (hsm-dbfx/use-sub-state :usb-serial)]
       (log/debug :terminal/layout (str "Rendering hsm state " serial-state))
       (if (hsm-dbfx/in-state serial-state :no-webserial)
         ($ webserial-unsupported-message)
         ($ term-widget-dbfx {:event-sub :serial-events :tx-event-id :serial-tx})))))

(defui layout []
  ($ :.flex.flex-row.gap-1
     ($ :.basis-80.grow-0 ($ side-panel))
     ($ :.basis-0.grow
        ($ terminal-pane))))
