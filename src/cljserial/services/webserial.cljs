(ns cljserial.services.webserial
  (:require
   [lambdaisland.glogi :as log]
   [statecharts.core :as hsm]
   [cljserial.utils.dbfx :as dbfx :refer [reg-event-fx inject-cofx reg-sub]]
   [cljserial.utils.hsm :as hsm-dbfx]
   [cljserial.utils.webserial :as wsi]))


;; == Schema =====================================================================
;; -- see cljserial.utils.term

;; ============================================================================
;; re-fx events and subscriptions

;; TODO: Inject the db path during initialisation instead maybe?

;; Define a standard set of interceptors for all serial port events
(def serial-event-interceptors
  ;; "path" interceptor: Update specified subsection of db rather than entire db
  [(dbfx/path [:serial-term :events])
   ;; Provide event timestamp as coeffect so our event handlers can be pure functions
   (inject-cofx :timestamp)])

(defn append-event [events {:keys [timestamp event-type bytes]}]
  (assoc events timestamp {:timestamp timestamp
                           :event-type event-type
                           :event-data {:byte-encoding :text
                                        :bytes bytes}}))

;; Clients transmit a :serial-tx event containg commands/data to be transmitted
;; This logs it to the app database, then generates an internal :webserial-tx event
;; that triggers the statemachine to transmit data via the port
;;
;; N.B. this may need adaptation to allow for coexistence with a bluetooth serial
;; connection.... TBD
(reg-event-fx
 :serial-tx
 serial-event-interceptors
 (fn [{:keys [db timestamp]} [_ bytes]]
   ;;DB effect: Append entry to the serial event database
   {:db (append-event db {:timestamp timestamp
                          :event-type :tx
                          :bytes bytes})
    ;;Coeffect - pass the request on to the statemachine to transmit
    :fx [[:dispatch [:webserial-tx bytes]]]}))

(reg-event-fx
 :serial-rx
 serial-event-interceptors
 (fn [{:keys [db timestamp]} [_event_id bytes]]
   ;;Append to an ongoing rx event...
   ;;As long data streams can be read over multiple rx events, if the previous recorded
   ;;comms event is rx, we assume this is an ongoing event and consolidate.
   ;;
   ;;N.B. This does NOT do any command terminator parsing -- that's a higher level concern
   ;;     to be dealt with by any subscribers to :webserial-rx
   (log/info :wsm/serial-rx bytes)
   (let [prev-event (last (vals db))
         ongoing (and (some? prev-event) (= (:event-type prev-event prev-event) :rx))
         ts (if ongoing (:timestamp prev-event) timestamp)
         data (if ongoing (str (:bytes (:event-data prev-event)) bytes) bytes)]
     ;;DB effect: Append entry to the serial event database
     {:db (append-event db {:timestamp ts
                            :event-type :rx
                            :bytes data})
      ;;Coeffect: Notify any downstream listeners (e.g. CD handlers) that new rx data has been added to the DB
      :fx [[:dispatch [:webserial-rx nil]]]})))

(reg-sub
 :serial-data
 (fn [db _]
   (:serial-term db))) ;;

(reg-sub
 :serial-events
 :<- [:serial-data]
 (fn [serial-state _]
   (:events serial-state))) ;;


;; ============================================================================
;; Controller - a statemachine
;; See https://lucywang000.github.io/clj-statecharts/

;;Initial context...
;; TODO Write a Schema?
;; ... also consider including event store in context here rather than separately in the refx db

(def default-context {:port nil
                      :serial-options wsi/DEFAULTS
                      :line-terminator "\r"})

(def controller
  (hsm/machine
   {:id :usb-serial
    :initial :disconnected
    :context default-context

    :states

    {;; TOP-LEVEL STATE
     :disconnected
     {:initial :webserial_pending
      :entry (hsm/assign (fn [ctx e]
                           (log/info :state/entry "Resetting port assignment")
                           (dbfx/dispatch [:webserial-disconnected])
                           (assoc ctx :port nil)))
      :on {:webserial-option {:actions
                              (hsm/assign
                               (fn [ctx {:keys [data]}]
                                 (let [key (first data)
                                       value-text (second data)
                                       ;; TODO: Use malli coercion here instead?
                                       value (if (re-matches #"\d+" value-text)
                                               (int value-text)
                                               (keyword value-text))]
                                   (log/debug :option/set (str key " : " value))
                                   (assoc-in ctx [:serial-options key] value))))}

           ;;FIXME: Some duplication here with webserial-has-port handler below...
           ;; :webserial-port-connected {:actions (hsm/assign (fn [ctx e]
           ;;                                                   (let [port (first (:data e))]
           ;;
           ;;                                                     (assoc ctx :port port))))
           ;;                            ;; :target :opening_port
           ;;                            }
           :webserial-port-opened :connected}
      :states
      {:webserial_pending
       {:entry (fn [ctx e]
                 (log/debug :state/entry (str "HSM INIT" ctx e))
                 (dbfx/dispatch [(if (wsi/is-supported?)
                                   :webserial-check-passed
                                   :webserial-check-failed)]))
        :on {:webserial-check-passed :port-pending
             :webserial-check-failed :no-webserial}}
       :no-webserial {}
       :port-pending
       {:entry (fn [ctx e]
                  ;; Ideally we'd do this, however webserial port request must be initiated via ui element click
                  ;; (wsi/await-port
                  ;;  :on-success #(dbfx/dispatch [:ui/event :webserial-has-port %1])
                  ;;  :on-failure #(dbfx/dispatch [:ui/event :webserial-no-port]))
                 (log/debug :state/entry (str "PORT PENDING" ctx e)))
        :on {:webserial-has-port {:actions (hsm/assign (fn [ctx e]
                                                         ;;The ports get passed through as a sequence...
                                                         (let [port (first (:data e))]
                                                           ;;TODO: Does this stack? I.e. end up with multiple triggering on disconnection/reconnection?
                                                           ;; (.addEventListener port "connect" #(dbfx/dispatch [:webserial-port-connected port]))
                                                           (.addEventListener port "disconnect" #(dbfx/dispatch [:webserial-port-disconnected port]))

                                                           (assoc ctx :port port))))
                                  :target :opening_port}
;;
             }}
       :opening_port
       {:entry (fn [ctx e]
                 (log/debug :state/entry (str "WAITING TO OPEN PORT" ctx))
                 (wsi/open-port (:port ctx)
                                :options (:serial-options ctx)
                                :on-success #(dbfx/dispatch [:webserial-port-opened])
                                :on-failure #(dbfx/dispatch [:webserial-port-open-failure])))
        :on {:webserial-port-open-failure {:actions (fn [ctx e] (log/error :port/open e))}}}}}

     ;; TOP-LEVEL STATE
     :connected
     {:entry (fn [ctx e]
               (dbfx/dispatch [:webserial-connected])
               (let [port (:port ctx)
                     port-id (wsi/describe-port port)]
                 (log/info :read/spawn-loop {:port-id port-id})
                 (wsi/go-read-text port #(dbfx/dispatch [:serial-rx %]))))
      :on {:webserial-tx
           {:actions (fn [context {:keys [data]}]
                       (let [{:keys [port line-terminator]} context
                             ;; The event parameters are wrapped in a vector - get first element
                             cmd (first data)]
                         (log/info :write/text cmd)
                         (wsi/write port (str cmd line-terminator))))}
           :webserial-forget-port :disconnecting
           :webserial-port-disconnected :disconnected}}

     ;; TOP-LEVEL STATE
     :disconnecting
     {:entry (fn [ctx e]
               (log/info :port/forget "TODO: Forget request received - IMPLEMENT ME")
               (let [port (:port ctx)
                     port-info (wsi/describe-port port)]
                 (wsi/forget-port port {:on-success #(dbfx/dispatch [:webserial-port-forgotten])
                                        :on-failure #(log/error :port/forget (str "Failed to forget " port-info))})))
      :on {:webserial-port-forgotten :disconnected}}

;; END TOP-LEVEL STATES
     }}))

(defn init []
  (hsm-dbfx/register controller))
