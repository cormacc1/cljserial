(ns cljserial.services.bt
  (:require
   [lambdaisland.glogi :as log]
   [statecharts.core :as hsm]
   [cljserial.utils.dbfx :as dbfx]
   [cljserial.utils.hsm :as hsm-dbfx]
   [cljserial.utils.bt :as bti]))

;; ============================================================================
;; Controller - a statemachine
;; See https://lucywang000.github.io/clj-statecharts/

;;Initial context...
;; TODO Write a Schema?
;; ... also consider including event store in context here rather than separately in the refx db

(def default-context {:device nil
                      :options bti/DEFAULT_OPTIONS
                      :line-terminator "\r"})

(def controller
  (hsm/machine
   {:id :bt-serial
    :initial :disconnected
    :context default-context
    :states {:disconnected {:on {:bt-connected :connected}}
             :connected {:on {:bt-disconnected :disconnected}}}}))

(defn init []
  (hsm-dbfx/register controller))
