(ns cljserial.services.mbt-cd
  (:require
   [clojure.string :as str]
   [malli.core :as m]
   [lambdaisland.glogi :as log]
   [statecharts.core :as hsm]
   [cljserial.utils.dbfx :as dbfx :refer [reg-sub]]
   [cljserial.utils.hsm :as hsm-dbfx]
   [cljserial.utils.term :as term]
   [cljserial.schema.version :refer [Version]]
   [cljserial.schema.filestore :refer [FileStore]]
   [cljserial.services.command-parser :as commands]))


;;---------------------------------------------------------------------------------------
;; Schema


(def CdState
  [:map
   [:serial :int]
   [:hardware-revision :int]
   [:firmware-revision Version]
   [:build-configuration :string]
   [:bluetooth-firmware :string]
   [:track :int]
   [:sd-card FileStore]])

(def initial-state
  {:serial 0
   :hardware-revision 0
   :firmware-revision {:major 0 :minor 0 :patch 0}
   :build-configuration ""
   :bluetooth-firmware ""
   :track 0
   :sd-card {:info {:capacity-mb 0 :used-mb 0}
             :files (sorted-map)}})

;;---------------------------------------------------------------------------------------
;; CD commands

(def line-terminator "\r\n")
(def response-terminator (str line-terminator "OK" line-terminator))
;;TODO: Rework this later to allow binary and text mode command handling...
(defn command-complete? [cmd resp]
  {:pre [(m/validate term/EventData cmd)
         (m/validate term/EventData resp)]}
  (str/ends-with? (:bytes resp) response-terminator))


;; TODO: Rework this and webserial/commands module to allow effects as well as db modification
;;       E.g. for file storage etc. we'll probably want to have coeffects to trigger file upload to S3 or whatever...
(def command-handlers
  [;; >> getid
   ;; MBT Controller #00000092 / HW v1 / FW v15.2.9
   ;; BT: Melody Audio V5.6 RC2 (MELODY_5x)
   ;; IOD 1601#0000112 / 145 hrs
   ;; OK
   {:id :getid
    :matcher #"getid"
    :db-subpath nil
    :response-parser
    (fn [db resp]
      (when-let [match (re-find #"MBT (\S+) #(\d+) / HW v(\d+) / FW v(\d+)\.(\d+)\.(\d+)" resp)]
        (dbfx/dispatch [:mbt-cd-identified]) ; FIXME: This should be done via event-fx instead
        (assoc db
               :build-configuration (get match 1)
               :serial (int (get match 2))
               :hardware-revision (int (get match 3))
               :firmware-revision
               {:major (int (get match 4))
                :minor (int (get match 5))
                :patch (int (get match 6))})))}
   ;; >> clin track
   ;; Selected TRACK01
   ;; OK
   {:id :clin-track
    :matcher #"clin track"
    :db-subpath nil
    :response-parser
    (fn [db resp]
      (when-let [match (re-find #"Selected TRACK(\d+)" resp)]
        (assoc db
               :track (int (get match 1)))))}
   ;; >> file ls
   ;; MUTEBUTT.LOG :: ---- ::      18092 :: 24/03/06 21:59:54
   ;; AKGK845B.CSV :: ---- ::        399 :: 24/03/01 14:02:00
   ;; Total Files: 2 (18 KB)
   ;; Used 0 of 7663 MB
   ;; OK
   {:id :file-ls
    :matcher #"file ls"
    :db-subpath nil
    :response-parser
    (fn [db resp]
      (when-let [du-match (re-find #"Used (\d+) of (\d+) MB" resp)]
        (dbfx/dispatch [:mbt-cd-files-listed])
        (let [lines (str/split-lines resp)
              file-lines (filter #(str/includes? % " :: ") lines)
              files (into (sorted-map) (for [f file-lines]
                                         (let [f-match (re-find #"(\S+)\s+::\s+(\S+)\s+::\s+(\d+)\s+::\s+(.+)" f)]
                                           [(get f-match 1) {:path (get f-match 1)
                                                             :attributes (get f-match 2)
                                                             :size (int (get f-match 3))
                                                             :timestamp (get f-match 4)}])))]
          (assoc db
                 :sd-card {:files files
                           :info {:capacity-mb (int (get du-match 2)) :used-mb (int (get du-match 1))}}))))}
   ;; END COMMAND HANDLERS
   ])

(comment
  (def file-ls-parser (:response-parser (get command-handlers 2)))
  (def file-ls-resp "MUTEBUTT.LOG :: ---- ::      18092 :: 24/03/06 21:59:54
AKGK845B.CSV :: ---- ::        399 :: 24/03/01 14:02:00
Total Files: 2 (18 KB)
Used 0 of 7663 MB
OK
")
  (file-ls-parser {} file-ls-resp)
  ;;
  )


;;---------------------------------------------------------------------------------------
;; Controller state machine
(def controller
  (hsm/machine
   {:id :mbt-cd
    :initial :disconnected
    ;;:context nil ;Consider moving CD state to statemachine context? Though think through subscriptions...
    :states {:disconnected {;;TODO: Entry handler -- reset state?
                            :on {:webserial-connected :connected}}
             :connected {:initial :identifying
                         :entry (fn [ctx e]
                                  (log/info :cd-hsm/connected "Yay we can run multiple state machines..."))
                         :on {:webserial-disconnected :disconnected}
                         :states {:identifying {:entry (fn [ctx e]
                                                         ;;TODO: associate the command with the handler above?
                                                         (dbfx/dispatch [:serial-tx "getid"]))
                                                :on {:mbt-cd-identified :listing-files}}
                                  :listing-files {:entry (fn [ctx e] (dbfx/dispatch [:serial-tx "file ls"]))
                                                  :on {:mbt-cd-files-listed :ready}}
                                  :ready {:entry (fn [ctx e] (log/info :cd-hsm/ready "MBT ready for action!"))}}}}}))



(defn init []
  ;; 1a. Initialise tokeniser with command copmletion indicator ("OK")
  ;;    When found, this causes an event to be published...
  (commands/set-exchange-tokeniser command-complete?)
  ;; 1b. ... that causes a command handler to be looked up to handle the data
  (commands/set-handlers command-handlers [:cd-info])
  ;; 2. Register the statemachine with dbfx (re-frame or refx, depending on which backend we're using)
  ;;    ... this causes an event listener to be registered for each event handled by the HSM.
  ;;    ... N.B. re-frame only allows one handler/owner per event, though as many listeners as you like
  ;;             can subscribe to the affected path of the re-frame db or whatever.
  (hsm-dbfx/register controller)

;; Subscription for cd state database updates...
;; TODO: Maybe we should do this in (commands/set-handlers instead?)
  (reg-sub
   :cd-info
   (fn [db _query-vector]
     (get-in db [:cd-info]))))
