(ns cljserial.components.file-card-scenes
 (:require
  [uix.core :refer [$]]
  [portfolio.react-18 :refer-macros [defscene]]
  [cljserial.schema.filestore :refer [FileStore FileStoreInfo File]]
  [cljserial.components.file-card :as file-card]
  [malli.generator :as mg]))

(defn fake-files
  "Generate a sorted map of `count` fake files."
  [count]
  (into (sorted-map) (for [file (mg/sample File {:size count})] [(:path file) file])))

(defn fake-file-store [file-count]
  {:files (fake-files file-count) :info (mg/generate FileStoreInfo)})

(defscene file-card-empty
  :title "File card - no files"
  ($ file-card/card (fake-file-store 0)))

(defscene file-card-one
  :title "File card - one file"
  ($ file-card/card (fake-file-store 1)))

(defscene file-card-ten
  :title "File card - 10 files"
  ($ file-card/card (fake-file-store 10)))


;; (defscene file-card-many
;;   :title "File card - multiple files"
;;   ($ file-card/card {:tasks (fake-tasks 5) :task-filter :all}))

;; (defscene file-card-generative
;;   :title "File card - random task-filter"
;;   ($ file-card/card (fake-task-store 10)))
